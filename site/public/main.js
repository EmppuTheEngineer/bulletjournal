window.onload = callFunctions;

function callFunctions() {
	var exitbutton = document.getElementById("exitbutton");
	var addbutton = document.getElementById("add");
	
	if(exitbutton != null) {
		exitbutton.onclick = function () {
			location.href = "../";
		};
	}
	
	if(addbutton != null) {
		addbutton.onclick = function () {
			location.href = "../add";
		};
	}
};