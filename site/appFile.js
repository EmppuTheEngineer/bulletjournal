var express = require('express');
var app = express();
var exphbs  = require('express-handlebars');
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

//dateformat setup
var dateformat = require('dateformat');
var now = new Date();
var today = dateformat(now, 'isoDate')
var currentlyViewedDate = new Date();
var dailyurl; //dateformat(currentlyViewedDate, 'isoDate');
var mode = 'daily';

//handlebars setup
app.engine('.hbs', exphbs({ 
        defaultLayout: 'main', 
        extname: '.hbs', // lyhyempi pääte
        helpers: { 
                section: function(name, options){ 
                        if(!this._sections) this._sections = {}; 
                        this._sections[name] = options.fn(this); 
                        return null; 
                } 
        } 
})); 
app.set('view engine', '.hbs');

//database setups
var MongoClient = require('mongodb').MongoClient; 
var db;
MongoClient.connect('mongodb://dummydummerson:MacDumFace@ds060749.mlab.com:60749/empputestaa', function(err, database) 
{
    if (err)
    {
        return console.log(err);
    };
    db = database;
});

app.set('port', process.env.PORT || 3000);

//Daily logs
//home == today, thou the daily generates also today by "/today-date"
app.get('/', function(req,res)
{
    res.render('daily', { layout: 'hometemplate' });

    fetchFromDatabase('tasks', today);
    fetchFromDatabase('events', today);
    fetchFromDatabase('notes', today);

    console.log('Home page downloaded correctly!');
});

//next
app.get('/next', function(req,res)
{
    currentlyViewedDate.setDate(currentlyViewedDate.getDate() + 1);
    dailyurl = dateformat(currentlyViewedDate, 'isoDate');
    res.redirect('/' + dailyurl);
});

//previous
app.get('/previous', function(req,res)
{
    currentlyViewedDate.setDate(currentlyViewedDate.getDate() - 1);
    dailyurl = dateformat(currentlyViewedDate, 'isoDate');
    res.redirect('/' + dailyurl);
});


//daily
app.get('/20**-**-**', function (req, res) {
  res.render('daily');
});

//daily
/*
app.get(('/' + dailyurl), function(req,res)
{
    res.render('huominen');
});
*/

app.get('/event', function(req, res) 
{ 
        res.render('event'); 
}); 

// tasks
app.get('/task', function(req, res) 
{ 
        res.render('task'); 
}); 

// notes
app.get('/note', function(req, res) 
{ 
        res.render('note'); 
}); 


//tiedomuokkaus
// adding a event/task/note
app.get('/add', function(req, res) 
{ 
    res.render('add'); 
}); 

// pathina sama, mihin formsissa menee, ei sama sivu, jossa kaikki tapahtuu!
app.post('/add_new', function(req, res)
{    
    //uusi merkintä
    var noteContent = req.body;

    //merkintätyyppi
    var noteType = noteContent.type;
    console.log('noteType:');
    console.log(noteType);

    //merkintätyypin poisto noteContentista
    delete noteContent.type;
    console.log('noteContent:');
    console.log(noteContent);

    //adding addedDate and addedTime
    noteContent.addedDate = dateformat(now, 'isoDate');
    noteContent.addedTime = dateformat(now, 'isoTime');

    console.log('noteContent:');
    console.log(noteContent);

    /*
    db.collection(noteType).save(noteContent, function(err, result) 
    {
        if (err) 
        {
            return console.log(err);
        }

        console.log('saved to ' + noteType + '-collection');
    })
    */

    res.redirect('/noteadded');
});

app.get('/noteadded', function(req, res)
{
    res.render('addednote');
})

// modify
app.get('/modify', function(req, res) { 
        res.render('modify'); 
}); 

// 404 - sivu puuttuu
app.use(function(req, res, next) 
{ 
        res.status(404); 
        res.render('404'); 
}); 

// 500 - palvelinvirhe
app.use(function(err, req, res, next) 
{ 
        console.log(err.stack); 
        res.type('text/plain'); 
        res.status(500); 
        res.render('500'); 
}); 

app.listen(app.get('port'), function()
{
    console.log('Expess started on http://localhost:' + app.get('port') + ', press Ctrl + C to quit.')
});


//fetch functions for database
//daily
function fetchFromDatabase(collection, date)
{
    db.collection(collection).find({startDate: date}).toArray(function(err, results) 
    {
        if(results[0] == null) //huom! === ei toimi, joten siksi ==
        {
            console.log('No events found for ' + date);
            res.send('No ' + collection + ' for this date!');
        }
        else
        {
            console.log(results);
            console.log('Fetched all items from ' + collection + " dated on " + date);
        }
    });
};

function fetchFromDatabase(collection)
{
    db.collection(collection).find().toArray(function(err, results) 
    {
        if(results[0] == null) //huom! === ei toimi, joten siksi ==
        {
            console.log('No events found for ' + date);
        }
        else
        {
            console.log(results);
            console.log('Fetched all items from ' + collection);
        }
    });
};